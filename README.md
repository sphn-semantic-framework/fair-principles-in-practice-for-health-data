# FAIR principles in practice for health data

This training is based on the SPHN RDF schema [2022.2](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/tree/2022-2) version. To view the newest version of the RDF schema or the template ontology, click [here](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology).

More information about the SPHN Semantic Interoperability Framework is available [here](https://sphn.ch/network/data-coordination-center/the-sphn-semantic-interoperability-framework/).
